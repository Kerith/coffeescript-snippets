
class LinkedList
  class Node
    constructor: (@left, @elem, @right) ->
      
  constructor: () ->
    @root = new Node(null, "Root", null)
    @size = 0

  insertLeft: (elem) ->
    node = new Node(null, elem, @root.left)
    @root.left.left = node
    @root.left = node

  insertRight: (elem) ->
    node = new Node(@root.right, elem, null)
    @root.right.right= node
    @root.right = node

  removeLeft: () ->
    @root.left = @root.left.right
    @root.left.left = null

  removeRight: () ->
    @root.right = @root.right.left
    @root.right.right = null

