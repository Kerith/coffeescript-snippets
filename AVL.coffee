class AVL
  class Node
    constructor: (@parent, @elem, @depth) ->
      @left = null
      @right = null

  constructor: (elem) ->
    @root = new Node(null, elem)

  insert: (elem) ->
    # Insert new node
    insertAux(elem, @root)

  insertAux: (elem, parent) ->
    if elem < parent.elem
      if parent.left == null
        parent.left = new Node(parent, elem, parent.depth + 1)
        return parent.left
      else
        balance(insertAux(elem, parent.left), "l")
    else if elem > parent.elem
      if parent.right == null
        parent.right = new Node(parent, elem, parent.depth + 1)
        return parent.right
      else
        balance(insertAux(elem, parent.right), "r")


  balance: (node, side) ->
    if node.left == null || node.right == null || Math.abs(node.left.depth - node.right.depth) <= 1
      return
    else # rebalance
      if side == 'l'
        ll = node.left.left unless node.left.left == null
        lr = node.left.right unless node.left.right == null

        if ll.depth - lr.depth == 1 # LL rotation
          rotateLL(node)
        else
          rotateLR(node)

      # WIP

